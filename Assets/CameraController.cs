﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{

    public Transform targetToFollow;
    public float followSpeed = 1;

    public Vector3 offset;

    // Use this for initialization
    void Start()
    {
        offset = targetToFollow.position - transform.position;

    }

    // Update is called once per frame
    void FixedUpdate()
    {
        transform.LookAt(targetToFollow.position);
        transform.position = Vector3.MoveTowards(transform.position, targetToFollow.position - offset, followSpeed * Time.deltaTime);
    }
}