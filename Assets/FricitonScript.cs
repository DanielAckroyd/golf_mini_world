﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FricitonScript : MonoBehaviour {

    [System.Serializable]
    public class RollingFrictionCoEff
    {
        public PhysicMaterial mat;
        public float drag = 0;
    }

    public List<RollingFrictionCoEff> coeffs = new List<RollingFrictionCoEff>();

    private PhysicMaterial prev;
    private Rigidbody rb;
	// Use this for initialization
	void Start () {
        rb = GetComponent<Rigidbody>();
	}
	
	// Update is called once per frame
	void Update () {

	}
    private void OnCollisionStay(Collision collision)
    {
        if (prev != collision.collider.material)
        {
            ResetDrag();

            var res = coeffs.Find(x => PhysicsMaterialCompare(x.mat, collision.collider.material));
            if (res != null)
            {
                rb.drag = res.drag;
                prev = res.mat;
            }
        }
    }
    private void OnCollisionExit(Collision collision)
    {
        ResetDrag();
    }

    void ResetDrag()
    {
        prev = null;
        rb.drag = 0.5f;
    }
    public static bool PhysicsMaterialCompare(PhysicMaterial lhs, PhysicMaterial rhs)
    {
        return lhs.dynamicFriction == rhs.dynamicFriction && lhs.staticFriction == rhs.staticFriction;
    }
}
