﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {

    public Rigidbody objectRigidBody;
    public float force;
    public float maxForce;
    public bool hittableState;
    // Use this for initialization
    void Start ()
    {
        objectRigidBody = GetComponent<Rigidbody>();
        hittableState = true;
	}
	
	// Update is called once per frame
	void FixedUpdate ()
    {

        var mousePos = Input.mousePosition;
        var camRay = Camera.main.ScreenPointToRay(mousePos);
        var playField = new Plane(Vector3.up, transform.position);

        float outDist = 0;
        if (playField.Raycast(camRay, out outDist))
        {
            var hitPos = camRay.GetPoint(outDist);

            Debug.DrawLine(transform.position, hitPos, Color.red);

            var dif = hitPos - transform.position;

            var aimDirection = dif.normalized;
            var aimPower = dif.magnitude * force;
            if (aimPower > maxForce)
            {
                aimPower = maxForce;
            }
            //Debug.Log(aimPower);
            if (Input.GetKeyDown("space") && (hittableState == true))
            {
                objectRigidBody.AddForce(aimDirection * aimPower);
                hittableState = false;
                //Debug.Log(aimDirection);
                
            }
            
            if (objectRigidBody.velocity.magnitude < 0.05)
            {
                objectRigidBody.velocity = Vector3.zero;
                hittableState = true;
                Debug.Log(objectRigidBody.velocity.magnitude);
            }

        }
    }
}
